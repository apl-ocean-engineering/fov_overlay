#!/usr/bin/env python3
#
# Publishes a sinusoidal signal on "/plane_z" for testing the projection node.
#
# Takes the following param:
#    - period_secs   : Period of sine
#    - amplitude
#    - offset
#    - publish_freq  : Frequency of topic publication

import rospy
import math
from std_msgs.msg import Float32
from greensea_msgs.msg import NavSolution


class SinePublisher:
    def __init__(self):
        self.sub = rospy.Subscriber("/raven/openins_nav", NavSolution, self.nav_cb )
        self.pub = rospy.Publisher("/plane_z", Float32, queue_size=1)

    def nav_cb(self, nav ):
        tank_depth = 3.0   # meters
        offset = 0.2  # reported depth at water surface
        self.pub.publish(
            tank_depth - (nav.depth - offset)
        )


if __name__ == "__main__":
    rospy.init_node("openins_depth_publisher", anonymous=True)

    _sp = SinePublisher()
    rospy.spin()
