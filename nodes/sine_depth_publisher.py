#!/usr/bin/env python3
#
# Publishes a sinusoidal signal on "/plane_z" for testing the projection node.
#
# Takes the following param:
#    - period_secs   : Period of sine
#    - amplitude
#    - offset
#    - publish_freq  : Frequency of topic publication

import rospy
import math
from std_msgs.msg import Float32


class SinePublisher:
    def __init__(self):
        self.pub = rospy.Publisher("/plane_z", Float32, queue_size=1)
        self.amplitude = rospy.get_param("~amplitude", 1)
        self.offset = rospy.get_param("~offset", 2)
        self.frequency = rospy.get_param("~publish_freq", 0.1)  # seconds

        self.period = rospy.get_param("period_secs", 5)  # Seconds
        self.period = self.period / (2 * math.pi)  # Prescale period

        self.timer = rospy.Timer(rospy.Duration(self.frequency), self.timer_cb)

    def timer_cb(self, timer_event):
        self.pub.publish(
            self.offset
            + self.amplitude * math.sin(timer_event.current_real.to_sec() / self.period)
        )


if __name__ == "__main__":
    rospy.init_node("sine_depth_publisher", anonymous=True)

    _sp = SinePublisher()
    rospy.spin()
