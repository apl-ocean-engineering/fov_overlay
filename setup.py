from distutils.core import setup
from catkin_pkg.python_setup import generate_distutils_setup

d = generate_distutils_setup(
    packages=["fov_overlay"],
    package_dir={"": "src"},
    console_scripts={"nodes/planar_overlay"},
)

setup(**d)
