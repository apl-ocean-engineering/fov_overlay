#!/usr/bin/env python3

import rospy
import numpy as np
import message_filters
import cv2
from cv_bridge import CvBridge

from sensor_msgs.msg import Image, CameraInfo
from std_msgs.msg import Float32


class PlanarOverlay:
    def __init__(self):
        self.fov_thickness = rospy.get_param("~fov_thickness", 3)
        self.fov_color = rospy.get_param("~fov_color", (0, 255, 0))
        self.fov_shape = rospy.get_param(
            "~fov_shape", "square"
        )  # Should be an enum?  Valid values as "square" and "circle"

        self.scale_x = rospy.get_param("~scale_x", 1.0)
        self.scale_y = rospy.get_param("~scale_y", 1.0)

        ## ~~ Hack hack ~~
        phoenix_width = 4024
        phoenix_height = 3036
        phoenix_cell_size = 1.85e-6  # In meters (IMX226 is 1.85um)
        phoenix_lens_f = 0.006  # In meters (6mm)

        # Fake focal length calculation (by similar triangles)
        #
        #  (sensor_size_{x,y}/2) / (lens focal length) = 1 / f

        f_x = phoenix_lens_f / phoenix_cell_size
        f_y = phoenix_lens_f / phoenix_cell_size

        self.fake_left_K = np.asarray(
            ((f_x, 0, phoenix_width / 2), (0, f_y, phoenix_height / 2), (0, 0, 1))
        )
        rospy.logwarn(f"Fake left K: {self.fake_left_K}")

        adcp_fov_deg = 6
        self.adcp_hfov_rad = np.deg2rad(adcp_fov_deg) / 2
        self.tan_adcp_hfov = np.tan(self.adcp_hfov_rad)
        adcp_f = 1 / self.tan_adcp_hfov

        self.fake_right_K = np.asarray(((adcp_f, 0, 0), (0, adcp_f, 0), (0, 0, 1)))
        # rospy.logwarn(f"Fake right K: {self.fake_right_K}")

        self.adcp_offset = np.asarray(
            ((0.1397, 0, 0))
        ).T  # 5.5"  in the +X dimension (from the CAD files)
        self.adcp_rot = np.identity(3)

        # Ignore below for now...
        # # Might be a function for making this cross product matrix
        # fake_tx = np.mat( (( 0, -fake_t[2], fake_t[1] ),
        #             ( fake_t[2], 0, fake_t[0]),
        #             ( -fake_t[1], fake_t[0], 0)) )

        # fake_E = fake_tx * fake_R

        # fake_F = np.linalg.inv( np.mat(self.fake_right_K).T ) * fake_E * np.linalg.inv(np.mat(self.fake_left_K))

        # rospy.logwarn(f"Fake E: {fake_E}")
        # rospy.logwarn(f"Fake F: {fake_F}")

        # self.F = fake_F

        ## ~~ End hackiness ~~

        self.plane_z_sub = rospy.Subscriber("/plane_z", Float32, self.plane_z_cb)

        self.image_sub = message_filters.Subscriber("/image_raw", Image)
        self.info_sub = message_filters.Subscriber("/camera_info", CameraInfo)
        self.ts = message_filters.TimeSynchronizer([self.image_sub, self.info_sub], 10)
        self.ts.registerCallback(self.image_cb)

        self.image_pub = rospy.Publisher("/image_out", Image, queue_size=10)

        self.plane_z = None

    def plane_z_cb(self, message):
        self.plane_z = message.data
        rospy.loginfo(f"Updated plane_z to {self.plane_z}")

    def image_cb(self, image_msg, camera_info):
        if self.plane_z is None:
            return

        rgb_image = CvBridge().imgmsg_to_cv2(image_msg, desired_encoding="rgb8")
        camera_info_D = np.array(camera_info.D)

        if len(camera_info.K) == 9 and camera_info.K[0] != 0.0:
            camera_info_K = np.ndarray(camera_info.K).reshape([3, 3])
        else:
            camera_info_K = self.fake_left_K

        # Undistort if coefficients are available
        if len(camera_info_D) > 0:
            rgb_undist = cv2.undistort(rgb_image, camera_info_K, camera_info_D)
        else:
            rgb_undist = rgb_image

        # Do all the math in the full scale image, then scale down to the scaled image
        #
        # This is the simplest, least mathematically rigorous form
        # Intersection of DVL "central beam" and floor of tank in ADCP frame
        adcp_cent = np.asarray([0, 0, self.plane_z])
        rospy.logdebug(f"ADCP center: {adcp_cent}")

        # Transform into camera frame
        adcp_cent_cam_frame = np.dot(self.adcp_rot, adcp_cent) + self.adcp_offset
        rospy.logdebug(f"FOV center world: {adcp_cent_cam_frame.T}")

        # Pproject
        adcp_cent_cam_image = np.dot(camera_info_K, adcp_cent_cam_frame)

        fov_center = [
            adcp_cent_cam_image[0] / adcp_cent_cam_image[2],
            adcp_cent_cam_image[1] / adcp_cent_cam_image[2],
        ]
        rospy.logdebug(f"FOV center pixels: {fov_center} px")
        fov_center = np.array(fov_center)

        # For now, assume cone is always normal to plane (do not compute conis projection)
        fov_radius_m = self.plane_z * self.tan_adcp_hfov
        adpt_fov_edge = np.array((0, fov_radius_m, self.plane_z))

        # Transform into camera frame
        adcp_fov_edge_cam_frame = (
            np.dot(self.adcp_rot, adpt_fov_edge) + self.adcp_offset
        )
        rospy.logdebug(f"FOV edge world: {adpt_fov_edge.T}")

        adcp_fov_edge_cam_image = np.dot(camera_info_K, adcp_fov_edge_cam_frame)
        fov_edge = np.array(
            [
                adcp_fov_edge_cam_image[0] / adcp_fov_edge_cam_image[2],
                adcp_fov_edge_cam_image[1] / adcp_fov_edge_cam_image[2],
            ]
        )
        rospy.logdebug(f"FOV edge pixels: {fov_edge} px")

        fov_radius_px = np.linalg.norm(fov_edge - fov_center, 2)
        rospy.logdebug(f"FOV radius {fov_radius_px} px")

        # Scale
        rgb_scaled = cv2.resize(rgb_undist, (0, 0), fx=self.scale_x, fy=self.scale_y)

        height, width, _ = rgb_undist.shape
        fov_center_scaled = (
            int(round(fov_center[0] * self.scale_x)),
            int(round(fov_center[1] * self.scale_y)),
        )

        ## \todo (fix if you're scaling unequally?)
        fov_radius_scaled = fov_radius_px * self.scale_x

        # Given the epipole we can solve:
        #
        #
        #

        if self.fov_shape == "square":
            rospy.logwarn("Square FOV unimplemented!")
        elif self.fov_shape == "circle":
            fov_rgb = cv2.circle(
                rgb_scaled,
                fov_center_scaled,
                int(round(fov_radius_scaled)),
                self.fov_color,
                int(self.fov_thickness),
            )

        self.image_pub.publish(CvBridge().cv2_to_imgmsg(fov_rgb, encoding="rgb8"))


def entrypoint() -> None:
    rospy.init_node("fov_overlay")

    _pp = PlanarOverlay()

    rospy.spin()
